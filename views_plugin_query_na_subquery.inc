<?php

/**
 * @file
 * Defines the default query object which optionally builds SQL to execute with db_rewrite_sql disabled and node_access included as a subquery
 */

/**
 * Extension of views_plugin_query_default
 */
class views_plugin_query_na_subquery extends views_plugin_query_default{

  function option_definition() {
    $options = parent::option_definition();
    $options['disable_sql_rewrite'] = array(
      'default' => 'enable',
    );
    $options['distinct'] = array(
      'default' => FALSE,
      'bool' => TRUE,
    );

    return $options;
  }

  /**
   * Add settings for the ui.
   */
  function options_form(&$form, &$form_state) {
    $form['disable_sql_rewrite'] = array(
      '#title' => t('SQL rewriting'),
      '#description' => t('Disabling SQL rewriting will disable node_access checks as well as other modules that implement hook_db_rewrite_sql().  Disabling SQL rewriting with node access subquery will include node access via a subquery.'),
      '#type' => 'select',
      '#default_value' => !empty($this->options['disable_sql_rewrite']) ? $this->options['disable_sql_rewrite'] : 'enable',
      '#prefix' =>  '<div class="messages warning">' . t('WARNING: Disabling SQL rewriting means that node access security is disabled, unless you select to include node access via subquery. This may allow users to see data they should not be able to see if your view is misconfigured. Please use this option only if you understand and accept this security risk.') . '</div>',
      '#options' => array('disable' => 'Disable SQL rewriting', 'na-subquery' => 'Disable SQL rewriting, but include node access via subquery', 'enable' => 'Leave SQL Rewriting Enabled'),
    );
    $form['distinct'] = array(
      '#type' => 'checkbox',
      '#title' => t('Distinct'),
      '#description' => t('This will make the view display only distinct items. If there are multiple identical items, each will be displayed only once. You can use this to try and remove duplicates from a view, though it does not always work. Note that this can slow queries down, so use it with caution.'),
      '#default_value' => !empty($this->options['distinct']),
    );
  }

  /**
   * Builds the necessary info to execute the proper query.
   */
  function build(&$view) {
    // Make the query distinct if the option was set.
    if (!empty($this->options['distinct'])) {
      $this->set_distinct();
    }

    // Store the view in the object to be able to use it later.
    $this->view = $view;

    $view->init_pager();

    // Let the pager modify the query to add limits.
    $this->pager->query();

    $this->query = $this->query();

    switch ($this->options['disable_sql_rewrite']) {
      case 'na-subquery':
        if (!node_access_view_all_nodes() && $view->base_field == 'nid' && $where = _node_access_where_sql()) {
          $this->where[] = array('clauses' => array($view->base_table . '.nid IN (SELECT _na_node.nid FROM {node} _na_node INNER JOIN {node_access} na ON _na_node.nid = na.nid WHERE ' . $where . ')'), 'args' => array(), 'type' => 'AND');
          $this->query = $this->query();
        }
      case 'disable':
        $this->final_query = $this->query;
        $this->count_query = $this->query(TRUE);
        break;
      case 'enabled':
      default:
        $this->final_query = db_rewrite_sql($this->query, $view->base_table, $view->base_field, array('view' => &$view));
        $this->count_query = db_rewrite_sql($this->query(TRUE), $view->base_table, $view->base_field, array('view' => &$view));
        break;
    }

    $this->query_args = $this->get_where_args();
  }
}