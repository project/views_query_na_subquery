<?php

/**
 * @file
 * Views API Implementations
 */

/**
 * Implementation of hook_views_plugins().
 */
function views_query_na_subquery_views_plugins() {
  $plugins = array(
    'query' => array(
      'views_query_na_subquery' => array(
        'title' => t('Node Access Subquery'),
        'help' => t('Allow views queries to be generated with db_sql_rewrite disabled and node_access included as a subquery.'),
        'handler' => 'views_plugin_query_na_subquery',
        'parent' => 'views_query',
      ),
    ),
  );

  return $plugins;
}

/**
 * Implementation of hook_views_data_alter().
 */
function views_query_na_subquery_views_data_alter(&$data) {
  foreach ($data as $table => &$table_data) {
    if (isset($table_data['table']['base'])) {
      // If query class is set and it contains views_query, we can swap it out.
      $is_views_query = isset($table_data['table']['base']['query class']) && ($table_data['table']['base']['query class'] == 'views_query');
      // If query class isn't set, we can assume that it's using views_query.
      if ($is_views_query || empty($table_data['table']['base']['query class'])) {
        $table_data['table']['base']['query class'] = 'views_query_na_subquery';
      }
    }
  }
}